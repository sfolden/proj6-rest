<html>
    <head>
        <title>CIS 322 Brevet time lists</title>
    </head>
    <body>

        <h1>Times:</h1>
        <br>
	<br>

        <form action="" method="post">
            <label for='time_options'>Choose times: </label>
            <select name="time_options">
                <option value="open_times">Open</option>
                <option value="close_times">Close</option>
                <option value="open_and_close_times">Open and Close</option>
            </select>



        <label for='html'>Format: </label>
            <select name="html">
                <option value="json">json</option>
                <option value="csv">csv</option>
            </select>


            <label for='top_num'>Top</label>
            <select name="top_num">
                <?php
                for ($i=1; $i<=20; $i++) {
                    ?>
                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
                    <?php
                }
                ?>
            </select>


        <input type="submit" name="button" value="Submit"/></form>
        <?php
        $time_choice = $_POST['time_options'];
        $top_val = $_POST['top_num'];
        $data_format = $_POST['html'];

       if($data_format=='json')
       {
            if($time_choice == "open_and_close_times")
            {
              echo '<h4>listAll</h4>';
              $response = 'http://laptop-service:5000/listAll' . '/json?top=' . $top_val;
	            $json =  file_get_contents($response);
	            $val = json_decode($json);
              $open_time = $val ->start_time;
              $close_time = $val ->end_time;

              #print times
              echo "Opening_time:\n";
                foreach($open_time as $var)
                {
                    echo "<li>$var</li>";
                }
              echo "Closing_time:\n";
                foreach($close_time as $var)
                {
                    echo "<li>$var</li>";
                }

         }
           if($time_choice == "open_times")
           {
              echo '<h4>listOpenOnly</h4>';
              $response = 'http://laptop-service:5000/listOpenOnly' . '/json?top=' . $top_val;
              $json =  file_get_contents($response);
              $val = json_decode($json);
              $open_time = $val ->start_time;

              echo "Opening_time:\n";
                foreach($open_time as $var)
                {
                    echo "<li>$var</li>";
                }

         }
          if($time_choice == "close_times")
          {
              echo '<h4>listCloseOnly</h4>';

              $response = 'http://laptop-service:5000/listCloseOnly' . '/json?top=' . $top_val;
              $json =  file_get_contents($response);
              $val = json_decode($json);

              $close_time = $val ->end_time;
              echo "closing_times:\n";
                foreach($close_time as $var)
                {
                    echo "<li>$var</li>";
                }

         }}else
         {
           # if json not picked, throw everything into CSV format
           if($time_choice=='open_and_close_times')
           {
                echo '<h4>listAll</h4>';
                $response = 'http://laptop-service:5000/listAll' . '/csv?top=' . $top_val;
                echo file_get_contents($response);

            }

            if($time_choice=='open_times')
            {
                echo '<h4>listOpenOnly</h4>';
                $response = 'http://laptop-service:5000/listOpenOnly' . '/csv?top=' . $top_val;
                echo file_get_contents($response);
            }

            if($time_choice=='close_times') {
                echo '<h4>listCloseOnly</h4>';
                $response = 'http://laptop-service:5000/listCloseOnly' . '/csv?top=' . $top_val;
                echo file_get_contents($response);
            }}
 ?>
</br>


</br>
    </body>
</html>
